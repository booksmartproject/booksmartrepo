import './App.css';
import React, { Component } from 'react';
class App extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			items: [],
			isLoaded: false,
		}
	}
	
	componentDidMount() {
		var url = 'https://openlibrary.org/isbn/9780140328721.json'
		fetch(url)
			.then(res => res.json())
			.then(json => {
				this.setState({
					isLoaded: true,
					items: json,
				})
			});
	}
	
	render() {
		
		var {isLoaded, items } = this.state;
		var bookISBN = '9780140328721'
		
		if (!isLoaded) {
			return <div> Loading ... </div>;
		}
		else {
			return (
				<div className="App">
					
					<textarea name="inputISBN" rows="1" defaultValue="9780140328721">
						
					</textarea>
					
					<button type="create" class="btn btn-primary start">
								<i class="glyphicon glyphicon-upload"></i>
								<span>Add Book</span>
					</button>
					
					
					
					<h2>Your Books</h2>
					<table>
					  <thead>
						<tr>
							<li key={items.publishers}>
								Name: {items.title} | Pages: {items.number_of_pages}
							</li>
						</tr>
					  </thead>
					  <tbody>         
					  </tbody>
					</table>
					<button type="create" class="btn btn-primary start">
								<i class="glyphicon glyphicon-upload"></i>
								<span>Submit Books</span>
					</button>
					<h2>Congrats you read this many pages!</h2>
					<h3> xyz pages per month </h3>
					<h3> xyz pages per week </h3>
					<h3> xyz pages per day </h3>
				</div>
			);
		}
	}
}

export default App;